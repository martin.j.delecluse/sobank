from django.db import models

from datetime import datetime

from .utils import geo_decode


class Customer(models.Model):
    """
    Model representing MiniBank's customers
    """
    id = models.AutoField(primary_key=True)
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    date_of_birth = models.DateField()

    address = models.CharField(max_length=200)
    # Address is converted to coordinates (lat, lon), which are stored in decimal fields.
    # Design choices:
    # - Allows null value in case computation of address' coordinates fails.
    # - Precision is set to 5 decimal places, meaning a coordinate resolution of ~1m.
    # - Positive latitude means North, negative means South.
    #   7 digits => from -90.00000 to +90.00000.
    # - Positive longitude means East, negative means West.
    #   8 digits => from -180.00000 to +180.00000.
    address_latitude = models.DecimalField(max_digits=7, decimal_places=5, null=True)
    address_longitude = models.DecimalField(max_digits=8, decimal_places=5, null=True)

    @staticmethod
    def _validate_json_input(json_data):
        """
        Method used to check json data used to build / update a customer instance.

        Will raise TypeError/ValueError if inputs are incorrect.
        """
        # First round of checks on input:
        # - must be a dictionary.
        if not isinstance(json_data, dict):
            raise TypeError(f"Expected a json dictionary. "
                            f"Got a {type(json_data)} instead.")
        # - must contain the required fields listed below.
        mandatory_fields = ['first_name', 'last_name', 'address', 'date_of_birth']
        missing_fields = [f for f in mandatory_fields if f not in json_data]
        if missing_fields:
            raise ValueError(f"Missing {missing_fields} parameter(s).")

        # Second round of check on inputs:
        # - verify that data is stored as 'str' for name & address fields.
        for field in ['first_name', 'last_name', 'address']:
            if not isinstance(json_data[field], str):
                raise TypeError(f"Expected a <str> for '{field}' parameter. "
                                f"Got a {type(json_data[field])} instead.")

    @classmethod
    def make_from_json(cls, json_data):
        """
        Build a Customer instance from a json dictionary.
        """
        cls._validate_json_input(json_data)

        # Try to extract the date of birth.
        # Will raise a TypeError or ValueError in case of issue.
        date_of_birth = datetime.strptime(json_data['date_of_birth'], "%Y-%m-%d")

        # Build the actual Customer object & update its coordinates
        customer = Customer(
            first_name=json_data['first_name'][:50],
            last_name=json_data['last_name'][:50],
            date_of_birth=date_of_birth,
            address=json_data['address'][:200]
        )
        customer.address_latitude, customer.address_longitude = geo_decode(customer.address)

        customer.save()
        return customer

    @classmethod
    def update_from_json(cls, json_data):
        """
        Update a Customer instance from a json dictionary.

        Requires the definition of all customer's field, including the 'id' field
        """
        cls._validate_json_input(json_data)
        # Method-specific check.
        if 'id' not in json_data:
            raise ValueError("Missing 'id' parameter.")

        # Try to extract the date of birth.
        # Will raise a TypeError or ValueError in case of issue.
        date_of_birth = datetime.strptime(json_data['date_of_birth'], "%Y-%m-%d")

        # Build the actual Customer object & update its coordinates
        customer_id = json_data['id']
        try:
            customer = Customer.objects.get(id=customer_id)
        except Customer.DoesNotExist:
            raise ValueError(f"No customer found with 'id'={customer_id}.")

        # Update the customer data (from fields present)
        customer.first_name = json_data['first_name'][:50]
        customer.last_name = json_data['last_name'][:50]
        customer.date_of_birth = date_of_birth
        customer.address = json_data['address'][:200]
        customer.address_latitude, customer.address_longitude = geo_decode(customer.address)

        customer.save()
        return customer


class Account(models.Model):
    """
    Model representing MiniBank's accounts
    """
    number = models.AutoField(primary_key=True, auto_created=True)
    owner = models.ForeignKey(Customer, on_delete=models.CASCADE)
    type = models.CharField(max_length=20)
    balance = models.FloatField()

    @staticmethod
    def _validate_json_input(json_data):
        """
        Method used to check json data used to build an account instance.

        Will raise TypeError/ValueError if inputs are incorrect.
        """
        # First round of checks on input:
        # - must be a dictionary.
        if not isinstance(json_data, dict):
            raise TypeError(f"Expected a json dictionary. "
                            f"Got a {type(json_data)} instead.")
        # - must contain the required fields listed below.
        mandatory_fields = ['owner_id', 'type', 'balance']
        missing_fields = [f for f in mandatory_fields if f not in json_data]
        if missing_fields:
            raise ValueError(f"Missing {missing_fields} parameter(s).")

        # Second round of check on inputs:
        # - verify that data is stored as 'int' for the 'owner' field.
        if not isinstance(json_data['owner_id'], int):
            raise TypeError(f"Expected a <int> for 'owner_id' parameter. "
                            f"Got a {type(json_data['owner_id'])} instead.")
        # - verify that data is stored as 'str' for the 'type' field.
        if not isinstance(json_data['type'], str):
            raise TypeError(f"Expected a <str> for 'type' parameter. "
                            f"Got a {type(json_data['type'])} instead.")
        # - verify that data is stored as 'float' for the 'balance' field.
        if not isinstance(json_data['balance'], float):
            raise TypeError(f"Expected a <float> for 'balance' parameter. "
                            f"Got a {type(json_data['balance'])} instead.")

        # Third round of check on inputs:
        # - verify that balance is positive.
        if json_data['balance'] < 0:
            raise ValueError("New accounts must be created with a positive balance.")

    @classmethod
    def make_from_json(cls, json_data):
        """
        Build an Account instance from a json dictionary.
        """
        cls._validate_json_input(json_data)

        # Get the customer for whom the account is created.
        customer_id = json_data['owner_id']
        try:
            customer = Customer.objects.get(id=customer_id)
        except Customer.DoesNotExist:
            raise ValueError(f"No customer found with 'owner_id'={customer_id}.")

        # Build the actual Customer object & update its coordinates.
        account = Account(
            owner=customer,
            type=json_data['type'],
            balance=json_data['balance']
        )

        account.save()
        return account


# Note: following methods could very well be in a class.
# It would allow to save all transactions in database.
def transfer_money(json_data):
    """
    Transfer money from one account to another
    """
    _check_input_consistency(json_data)

    # Get the accounts to update.
    origin_number = json_data['account_origin']
    destination_number = json_data['account_destination']
    try:
        origin_account = Account.objects.get(number=origin_number)  # type(Account)
    except Account.DoesNotExist:
        raise ValueError(f"No origin account found with number={origin_number}.")
    try:
        destination_account = Account.objects.get(number=destination_number)
    except Account.DoesNotExist:
        raise ValueError(f"No destination account found with number={destination_number}.")

    # Verify the account balance.
    if origin_account.balance < json_data['amount']:
        raise ValueError(f"Balance insufficient. Only {origin_account.balance} available.")

    # Perform the transaction
    origin_account.balance -= json_data['amount']
    destination_account.balance += json_data['amount']

    # Save the customer data
    origin_account.save()
    destination_account.save()
    return


def _check_input_consistency(json_data):
    """
    Check the consistency of the inputs for the 'transfer_money' function
    """
    # First round of checks on input:
    # - must be a dictionary.
    if not isinstance(json_data, dict):
        raise TypeError(f"Expected a json dictionary. "
                        f"Got a {type(json_data)} instead.")
    # - must contain the required fields listed below.
    mandatory_fields = ['account_origin', 'account_destination', 'amount']
    missing_fields = [f for f in mandatory_fields if f not in json_data]
    if missing_fields:
        raise ValueError(f"Missing {missing_fields} parameter(s).")

    # Second round of check on inputs:
    # - verify that data is stored as 'int' for the 'origin' & 'destination' fields.
    for field in ['account_origin', 'account_destination']:
        if not isinstance(json_data[field], int):
            raise TypeError(f"Expected a <str> for '{field}' parameter. "
                            f"Got a {type(json_data[field])} instead.")
    # - verify that data is stored as 'float' for the 'amount' field.
    if not isinstance(json_data['amount'], float):
        raise TypeError(f"Expected a <float> for 'amount' parameter. "
                        f"Got a {type(json_data['amount'])} instead.")
    # - verify that amount to transfer is positive.
    if json_data['amount'] < 0:
        raise ValueError(f"Amount to transfer must be a positive number..")